import React from 'react'


// The code starts by declaring the constructor function.
// The constructor takes in a single parameter, props.
// This is an object that contains all of the data for this component.
// The code is meant to illustrate how a form component might be implemented.
// The constructor takes in the props object and assigns it to this.state .
class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employee_number: '',
    };
    // The handleNameChange function is called when the user changes their name.
    this.handleNameChange = this.handleNameChange.bind(this);
    // The handleEmployerNumberChange function is called when the user changes their employer number.
    this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
    // Finally, the handleSubmit function is called when they submit the form.
    this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({employee_number: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // console.log(data);

        const salesPersonUrl = 'http://localhost:8090/api/sales_person/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson);

            const cleared = {
                name: '',
                employee_number: '',
            }
            this.setState(cleared)
        }
    }

render() {
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Enter a Sales Person</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name"
                    required type="text" name="name"
                    id="name" className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleEmployeeNumberChange} placeholder="employee_number"
                    required type="text" name="employee_number"
                    id="employee_number" className="form-control" value={this.state.employee_number}/>
                    <label htmlFor="employee_number">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }
}
        export default SalesPersonForm;
