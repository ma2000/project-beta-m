import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './Sales_service/SalesPersonForm';
import CustomerForm from './Sales_service/CustomerForm';
import SalesRecordForm from './Sales_service/SalesRecordForm';
import SalesRecordList from './Sales_service/SalesRecordList';
import './index.css';
import TechnicianForm from './Service/TechnicianForm'
import ServiceForm from './Service/ServiceForm'
import ActiveAppointmentList from './Service/ActiveAppointmentList'
import ServiceHistory from './Service/ServiceHistory'
import ManufacturerList from './Inventory/ManufacturerList';
import ModelList from './Inventory/ModelList';
import AutomobileList from './Inventory/AutomobileList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelForm from './Inventory/ModelForm';
import AutomobileForm from './Inventory/AutomobileForm';

function App() {
  const [appointments, setAppointments] = useState([])
  const [technicians, setTechnicians] = useState([])
  const [filteredVin, setFilteredVin] = useState('')
  const [manufacturers, setManufacturers] = useState([])
  const [models, setModels] = useState([])
  const [automobiles, setAutomobiles] = useState([])

  useEffect(() => {
    getAppointments()
    getManufacturers()
    getModels()
    getAutomobiles()
    getTechnicians()
  }, [])

  async function getManufacturers() {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url)
    const manufacturers = await response.json()
    if (response.ok) {
      setManufacturers(manufacturers)
    }
  }

  async function getModels() {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url)
    const models = await response.json()
    if (response.ok) {
      setModels(models)
    }
  }

  async function getAutomobiles() {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url)
    const automobiles = await response.json()
    if (response.ok) {
      setAutomobiles(automobiles)
    }
  }

  async function getTechnicians() {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url)
    const technicians = await response.json()
    if (response.ok) {
      setTechnicians(technicians)
    }
  }

  async function getAppointments() {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url)
    const appointments = await response.json()
    if (response.ok) {
      setAppointments(appointments)
    }
  }

  function handleVinFilterSubmit(vin) {
    setFilteredVin(vin)
  }

  function filterAppointmentsActive() {
    if (appointments.appointments !== undefined) {
      return appointments.appointments.filter((appointment) => (appointment.active) ? appointment : null)
    }
    else {
      return []
    }
  }

  function filterAppointmentsVin() {
    if (filteredVin !== '') {
      return appointments.appointments.filter((appointment) => (appointment.vin === filteredVin) ? appointment : null)
    }
    else {
      return []
    }
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/">
            <Route path="" element={<MainPage />} />
            <Route path="manufacturers">
              <Route path="list" element={<ManufacturerList manufacturers={manufacturers} />} />
              <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
            </Route>
            <Route path="models">
              <Route path="list" element={<ModelList models={models} />} />
              <Route path="new" element={<ModelForm getModels={getModels} manufacturers={manufacturers} />} />
            </Route>
            <Route path="automobiles">
              <Route path="list" element={<AutomobileList automobiles={automobiles} />} />
              <Route path="new" element={<AutomobileForm getAutomobiles={getAutomobiles} models={models} />} />
            </Route>
            <Route path="technicians">
              <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians} />} />
            </Route>
            <Route path="appointments">
              <Route path="new" element={<ServiceForm getAppointments={getAppointments} technicians={technicians} />} />
              <Route path="active" element={<ActiveAppointmentList appointments={filterAppointmentsActive()} getAppointments={getAppointments} />} />
              <Route path="servicehistory" element={<ServiceHistory appointments={filterAppointmentsVin()} handleFilterSubmit={handleVinFilterSubmit} filteredVin={filteredVin} />} />
            </Route>
            <Route path="/salesperson/new" element={<SalesPersonForm />} />
            <Route path="/customers/new" element={<CustomerForm />} />
            <Route path="sales_record">
              <Route index element={<SalesRecordList />} />
              <Route path="new" element={<SalesRecordForm />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
