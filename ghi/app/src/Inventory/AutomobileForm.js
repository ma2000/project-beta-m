import React, { useState } from 'react';

const AutomobileForm = ({ getAutomobiles, models }) => {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [automobileModel, setAutomobileModel] = useState('')

    const postAutomobile = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                color: color,
                year: year,
                vin: vin,
                model_id: automobileModel
            }),
            header: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const successTag = document.getElementById('success')
            const formTag = document.getElementById('form')
            successTag.classList.remove("d-none")
            formTag.classList.add("d-none")
        }
    }

    const handleSubmit = e => {
        e.preventDefault()
        postAutomobile()
            .then(() => {
                getAutomobiles()
            })
    }

    if (models.models !== undefined) {
        return (
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <div className="alert alert-success d-none" role="alert" id="success">
                        <h4 className="alert-heading">Automobile Successfully Created!</h4>
                    </div>
                    <div className="shadow p-4 mt-4" id="form">
                        <h1>Add a New Automobile</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input value={color} onChange={(event) => setColor(event.target.value)} placeholder="color" required type="text" name="color" id="color"
                                    className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={year} onChange={(event) => setYear(event.target.value)} placeholder="year" required type="number" name="year" id="year"
                                    className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={vin} onChange={(event) => setVin(event.target.value)} placeholder="vin" required type="text" name="vin" id="vin"
                                    className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select value={automobileModel} onChange={(event) => setAutomobileModel(event.target.value)} required id="automobileModel" name="automobileModel" className="form-select">
                                    <option value="">Choose a Model</option>
                                    {models.models.map(model => {
                                        return <option key={model.id} value={model.id}>{model.name}</option>
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return null
    }
}
export default AutomobileForm