import React, { useState } from 'react';

const ManufacturerForm = ({ getManufacturers }) => {
    const [manufacturerName, setManufacturerName] = useState('')

    const postManufacturer = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                name: manufacturerName
            }),
            header: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const successTag = document.getElementById('success')
            const formTag = document.getElementById('form')
            successTag.classList.remove("d-none")
            formTag.classList.add("d-none")
        }
    }

    const handleSubmit = e => {
        e.preventDefault()
        postManufacturer()
            .then(() => {
                getManufacturers()
            })
    }

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="alert alert-success d-none" role="alert" id="success">
                    <h4 className="alert-heading">Manufacturer Successfully Created!</h4>
                </div>
                <div className="shadow p-4 mt-4" id="form">
                    <h1>Add a New Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input value={manufacturerName} onChange={(event) => setManufacturerName(event.target.value)} placeholder="manufacturerName" required type="text" name="manufacturerName" id="manufacturerName"
                                className="form-control" />
                            <label htmlFor="manufacturerName">Name</label>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default ManufacturerForm