import React, { useState } from 'react';

const ServiceForm = ({ getAppointments, technicians }) => {
    const [vin, setVin] = useState('')
    const [customerName, setCustomerName] = useState('')
    const [date, setDate] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')

    const postAppointment = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                vin: vin,
                owner: customerName,
                date: date,
                technician: technician,
                reason: reason
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            const successTag = document.getElementById('success')
            const formTag = document.getElementById('form')
            successTag.classList.remove("d-none")
            formTag.classList.add("d-none")
        }
    }

    const handleSubmit = e => {
        e.preventDefault()
        postAppointment()
            .then(() => {
                getAppointments()
            })
    }

    if (technicians.technicians !== undefined) {
        return (
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <div className="alert alert-success d-none" role="alert" id="success">
                        <h4 className="alert-heading">New Appointment Successfully Created!</h4>
                    </div>
                    <div className="shadow p-4 mt-4" id="form">
                        <h1>Create a New Service Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input value={vin} onChange={(event) => setVin(event.target.value)} placeholder="vin" required type="text" name="vin" id="vin"
                                    className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={customerName} onChange={(event) => setCustomerName(event.target.value)} placeholder="customerName" required type="text" name="customerName" id="customerName"
                                    className="form-control" />
                                <label htmlFor="owner">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={date} onChange={(event) => setDate(event.target.value)} placeholder="date" required type="datetime-local" name="date" id="date"
                                    className="form-control" />
                                <label htmlFor="date">Date & Time</label>
                            </div>
                            <div className="mb-3">
                                <select value={technician} onChange={(event) => setTechnician(event.target.value)} required id="technician" name="technician" className="form-select">
                                    <option value="">Choose a technician</option>
                                    {technicians.technicians.map(technician => {
                                        return <option key={technician.employee_number} value={technician.employee_number}>{technician.name} -- employee#{technician.employee_number}</option>
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="reason" className="form-label" id="reason">Reason</label>
                                <textarea value={reason} onChange={(event) => setReason(event.target.value)} className="form-control" id="reason" rows="3"></textarea>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return null
    }
}
export default ServiceForm