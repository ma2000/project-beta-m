# CarCar

Team:

*
Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.



----------------------------------------------------

This is a Car Service application for dealerships to manage their Customers, Sales Records, Employees, and Services.
To begin working on the sales side of the application, you must first create a manufacturer, then a vehicle model, and finally an automobile using the two previous fields: color and year.
Customers must be created, as well as a salesperson, in order to create a Sales Record (the Sales Record also requires an automobile to be created).
Sales Records can be viewed in their entirety or filtered by salespeople using Case-Sensitive text searches.


----------------------------------------------------

From the reading in the exploration, "The Sales Microservice" consists of four models which I’ve implemented below:
----------------------------------------------------
1. AutomobileVO: This model handles the integration with the inventory microservice
    by creating a value object with the unique property of "vin". This allows me
    to pull the individual values of "automobile" from the inventory microservice,
    making it possible to make a foreign key relationship between the SalesRecord
    and Automobile, from inventory_rest. We accomplish this by making a function
    that will poll the inventory microservice for information on automobiles, using
    the vin number specifically.

2. Customer: The customer model allows us to create a customer using three
    properties: name, address, and phone number. This will also relate to
    SalesRecord.

3. SalesPerson: The SalesPerson model allows us to create a sales person with two
    properties: name, employee_number, and sales_record. This relates to
    SalesRecord.

4. SalesRecord: The SalesRecord model allows us to create a sales record using the
    three other models as ForeignKey relationships, with the additional property of
    sales_price.

----------------------------------------------------
AutomobileVO will poll data from the inventory microservice via poller.py. The data that we need from the inventory microservice is the vin number so that we can compare vin numbers in the inventory to vin numbers in appointments.

----------------------------------------------------

The design of the app was to maintain simplicity at its core. We agreed on terms that would be as verbose as possible without making them too bloated: Customer, AutomobileVO, Appointment, Technician, SalesPerson, and SalesRecord. By utilizing promises rather than a large number of componentDidMount calls, we attempted to keep the React portion of the app as lightweight as possible in terms of the code; Dry code is best.

Based on the high dependency of "Auto Sales" on "Inventory", those should be in one bounded context with the aggregate root being the "Inventory" and the "Auto Sales" being an aggregate. This was decided because "Inventory" can exist on its own while "Auto Sales" is directly dependent on "Inventory" in order to function. "Automobile Services" can function on its own, so it's a root.

----------------------------------------------------
When a user opens the site, from the main page, they should have dropdowns to each category that they have access to; sales and inventory.
Off of the sales dropdown, they should be able to:

    Show a Sales List -> Sales History
    Get the Sales History of a Salesperson -> Individual Performance
    Create a New Salesperson -> Add New Employee
    Create a New Customer -> Add New Customer
    Create a New Sales Record -> Create a New Sale

Off of the inventory dropdown, they should be able to:

    Show Automobiles -> Automobile Inventory
    Create Automobiles -> Add an Automobile
    Show Manufacturers -> Manufacturers
    Create Manufacturers -> Add a Manufacturer
    Show Vehicle Models -> Vehicle Models
    Create Vehicle Models -> Add a Vehicle Model

----------------------------------------------------
Hope you enjoy our project!
----------------------------------------------------

![](ghi/app/public/Project.png)
![](ghi/app/public/dia%202.png)
